package dssdo.kotlin.examples.`1`

fun printDiv(a: Int, b: Int): Unit {
    println("div of $a and $b is ${1.0*a/b}")
}

fun maxOf(a: Int, b: Int) = if (a > b) a else b

fun main(args : Array<String>) {
    printDiv(34, 78)
    printDiv(b = 9, a = 7)

    println("max is ${maxOf(-7, -9)}")

    val text = """
    |Tell me and I forget.
    |Teach me and I remember.
    |Involve me and I learn.
    |(Benjamin Franklin)
    """.trimMargin()
    println(text)
}