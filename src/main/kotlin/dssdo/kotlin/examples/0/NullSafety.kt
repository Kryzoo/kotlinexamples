package dssdo.kotlin.examples.`0`

import interoperability.java.ExamplesFromJava


fun main (s: Array<String>) {

    /* 1 */
    //nullable types
    var nullableString: String? = "abc"
    nullableString = null // ok
    println(nullableString)

    /* 2 */
    //not nullable types
    var notNullableString: String = "defg"
    //notNullableString = null

    /* 3 */
    //types from Java are all nullable
    val loe = ExamplesFromJava()
// check this:
//    val str: String = loe.element
    val str: String? = loe.nullElement

    /* 4 */
    println(str?.length)

    //Elvis operator
    println("Default Elvis: ${str?.length ?: -1}") //if (b != null) b.length else -1

    /* 5 */
    //we want to use nullable anyway
    //we cannot this:
    // notNullableString = str
    //notNullableString = str!!

    /* 6 */
    val aa: Any = "ddd"
    val a: Int? = aa as? Int
    println("safe cast result: ${a}")

    /* 7 */
    println("sum ${sum(null, null)}")

    /* 8 */
    //real life
    val req1: Request? = Request(Message("request1"))
    val req2: Request? = Request(null)
    val req3: Request? = null
    println("source from request and message: ${req1?.message?.source}")
    println("source from request and message: ${req2?.message?.source}")
    println("source from request and message: ${req3?.message?.source}")
}

fun sum(a: Int?, b: Int?): Int = (a?:0) + (b?:0)

data class Message (
    var source: String?
)
data class Request (
    var message: Message?
)