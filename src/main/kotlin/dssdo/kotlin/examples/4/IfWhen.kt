package dssdo.kotlin.examples.`4`

fun isOdd(x: Int) = if (x % 2 == 1) "Odd" else "Even"

fun writeWharXIs(x: Int) = when (x) {
    1 -> "x == 1"
    2 -> "x == 2"
    else -> "x is neither 1 nor 2"
}

fun main (s: Array<String>) {

    for (i in 0..10)
        println("${i} is ${isOdd(i)}")

    println("${writeWharXIs(21)}")
}