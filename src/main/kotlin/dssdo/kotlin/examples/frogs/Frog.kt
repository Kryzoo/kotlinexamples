package dssdo.kotlin.examples.frogs

class Frog(val places: CharArray = charArrayOf('A', 'B', 'C', 'D', ' ', '1', '2', '3', '4')) {
    val finalPlaces: CharArray = charArrayOf('1', '2', '3','4', ' ', 'A', 'B', 'C', 'D')
    val directions: HashMap<Char, Int> = hashMapOf('A' to 1, 'B' to 1, 'C' to 1, 'D' to 1, '1' to -1, '2' to -1, '3' to -1, '4' to -1)

    fun size(): Int = places.size

    fun print() {
        println(places.joinToString())
    }

    private fun move(pos: Int, length: Int): Frog? {
        if (places[pos] == ' ')
            return null
        val newPos: Int = pos + length * directions.get(places[pos])!!
        if (newPos < 0 || newPos >= places.size)
            return null
        if (places[newPos] != ' ')
            return null
        val newPlaces = places.copyOf()
        newPlaces[newPos] = places[pos]
        newPlaces[pos] = ' '
        return Frog(newPlaces)
    }

    fun forward(pos: Int): Frog? {
        return move(pos, 1)
    }

    fun jump(pos: Int): Frog? {
        return move(pos, 2)
    }

    fun stopCondition(): Boolean {
        return finalPlaces.contentEquals(places)
    }
}

fun frogGameTurn(f: Frog): ArrayList<Frog>? {
    for(pos in 0 .. (f.size() - 1)) {
        val fs = listOf(f.forward(pos), f.jump(pos))
        for (f in fs) {
            if (f != null) {
                if (f.stopCondition()) {
                    return arrayListOf(f)
                }
                val hist = frogGameTurn(f)
                if (hist != null) {
                    hist.add(0, f)
                    return hist
                }
            }
        }
    }
    return null;
}

fun main(args : Array<String>) {
    Frog().print()
    val hist = frogGameTurn(Frog())
    if (hist == null)
        println("failed")
    else
        for(l in hist)
            l.print()
}