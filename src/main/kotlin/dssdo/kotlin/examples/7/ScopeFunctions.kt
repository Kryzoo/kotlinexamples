package dssdo.kotlin.examples.`7`

import interoperability.java.ExamplesFromJava

data class Person(
    val name: String,
    var age: Int,
    var city: String
) {
    fun moveTo(newCity: String) {
        city = newCity;
    }

    fun incrementAge() {
        age += 1;
    }

}

fun main (s: Array<String>) {
    val alice1 = Person("Alice1", 20, "Amsterdam")
    println(alice1)
    alice1.moveTo("London")
    alice1.incrementAge()
    println(alice1)

    println()

    //let
    val alice2 = Person("Alice2", 20, "Amsterdam").let {
        println(it)
        it.moveTo("London")
        it.incrementAge()
        println(it)
        it
    }
    println("Alice2: ${alice2}")
    println()

    //often used in this way
    val el = ExamplesFromJava().element
    el?.let {
        println("when not null, length is ${it.length}\n")
    }

    //=============================================================
    //let vs run
    val str = "Hello"
    val len1 = str.run {
        println("The string's length: $length")
        length
    }
    val len2 = str.let {
        println("The string's length is ${it.length}")
        it.length
    }
    println("lengths: $len1, $len2")

    //=============================================================
    //run without object context
    val number = run {
        2*2
    }
    println("run calculated: $number\n")

    //=============================================================
    //with
    val numbers = mutableListOf("one", "two", "three")
    val last = with(numbers) {
        val firstItem = first()
        val lastItem = last()
        println("First item: $firstItem, last item: $lastItem")
        lastItem
    }
    println("with result: $last\n")


    //=============================================================
    //apply
    val adam = Person("Adam", 41, "Warsaw").apply {
        incrementAge()
        moveTo("Otwock")
    }
    println("Adam apply: $adam\n")

    //=============================================================
    //also
    val marek = Person("Marek", 33, "Warsaw").also {
        it.incrementAge()
        it.moveTo("Pruszków")
    }
    println("Marek also: $marek\n")

    val henryka = Person("Henryka", 69, "Hel")
        .also { println("${it.name} is ${it.age} and lives in ${it.city}") }
        .apply { moveTo("Jurata") }
        .also { println("she moved to ${it.city}\n") }

    println("Henia: $henryka\n")
}