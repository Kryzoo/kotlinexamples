package dssdo.kotlin.examples.`6`

fun doOperation(arg1: Int, arg2: Int, operation: (Int, Int) -> String): String {
    return operation(arg1, arg2)
}

fun sumText (a: Int, b: Int): String = "$a + $b = ${a+b}"

//infix notation
infix fun Int.divides(x: Int): Boolean = this % x == 0

fun main (s: Array<String>) {
    println(
            doOperation(3, 6, {a, b -> "$a - $b = ${a-b}"})
    )
    println(
            doOperation(3, 6, ::sumText)
    )

    val a = 45
    val b = 8
    println("$a divides $b: ${a divides b }") // a.divides(b)
}