package dssdo.kotlin.examples.`5`

data class Address (val street: String,
                    val number: Int,
                    var city: String)



fun main (s: Array<String>) {
    val a = Address("Topiel", 12, "Warszawa")
    println(a)

    //deconstruct
    val (street, number, city) = a

    println("\n\nstreet: ${street} , number: ${number}, city: ${city}")

}
