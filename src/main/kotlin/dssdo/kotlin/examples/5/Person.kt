package dssdo.kotlin.examples.`5`

import java.lang.Integer.max

data class Person (val firstName: String,
              val sureName: String,
              val age: Int = 42,
              val address: Address) {
}

fun main (s: Array<String>) {

    val person = Person(firstName = "John",
            address=Address(street = "Topiel", number = 12, city = "Warszawa"),
            sureName = "Doe")

    println(person)

    val person2 = person.copy(age = 34)
    println(person2)

}

