package dssdo.kotlin.examples

import java.math.BigDecimal

interface Insurance {
    val insuranceName: String
    val insurancePrice: BigDecimal
}

data class Rpo(override val insuranceName: String,
               override val insurancePrice: BigDecimal) : Insurance

data class Assistance(override val insuranceName: String,
                      override val insurancePrice: BigDecimal) : Insurance

data class Item(val productName: String, val materialIndex: String = "nope", val price: BigDecimal)

data class Cart(val items: List<Item>, val rpo: Rpo, val ass: Assistance) {

    init {
        println("Initializer block is here ${this}")
    }

    constructor(items: List<Item>) : this(
        items,
        Rpo("(empty)", BigDecimal.valueOf(0)),
        Assistance("(empty)", BigDecimal.valueOf(0))
    )

    constructor(rpo: Rpo, ass: Assistance) : this(emptyList(), rpo, ass)

    override fun toString(): String {
        return """=====================================
    Items: ${items}
    Rpo: ${rpo}
    Assistance: ${ass}
"""
    }

    fun totalPrice(): BigDecimal = items.fold(BigDecimal.ZERO) { acc, e -> acc + e.price }

    fun maxPrice(): BigDecimal = items.fold(BigDecimal.ZERO) { acc, e -> if (acc > e.price) acc else e.price }
}

fun main (s: Array<String>) {
    val a = Cart(listOf(
        Item(productName = "tv1", price = BigDecimal.ONE),
        Item(productName = "rv1", price = BigDecimal.TEN)))
    println(a)
    println(a.totalPrice())
}

