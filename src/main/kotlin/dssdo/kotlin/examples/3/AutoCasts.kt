package dssdo.kotlin.examples.`3`

fun main (s: Array<String>) {
    val v: Any = "a string as Any"

    if (v is String) {
        println("size of >>${v}<< is ${v.length}")
    }
}