package dssdo.kotlin.examples

import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

internal class CartTest {

    @Test
    fun test1() {
        val item1 = Item("prod1", "INDEX1", BigDecimal.valueOf(123.33))
        val item2 = Item("prod2", "INDEX2", BigDecimal.valueOf(223.56))
        val item3 = Item(productName = "prod3", price = BigDecimal.valueOf(323.35))

        val items = listOf(item1, item2, item3)
        val ass = Assistance("assistance", BigDecimal.valueOf(43.67))
        val rpo = Rpo("rpo", BigDecimal.valueOf(25.94))
        val cart = Cart(items, rpo, ass)

        val item4 = item1.copy(price = BigDecimal.ONE, productName = "prod4")
        val cart2 = cart.copy(items = listOf(item4) + items.drop(1))

        println (cart)
        println (cart2)

        println (cart.totalPrice())
        assertEquals(cart.items[0].price + cart.items[1].price + cart.items[2].price, cart.totalPrice())

        println ("Max price ${cart.maxPrice()}")
    }
}